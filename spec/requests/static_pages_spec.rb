require 'spec_helper'

describe "Static Pages" do
  

  subject {page}

  shared_examples_for "all static pages" do
    it { should have_selector('h1', text: heading) }
    it { should have_selector('title', text: full_title(page_title)) }
  end

  describe "Home Page" do
    before { visit root_path }
  	let(:heading) { 'Gabe\'s App'}
    let(:page_title) { '' }
    it { should_not have_selector('title', :text => 'Home') }
  end
  describe "Help Page" do
    before { visit help_path }
  	let(:page_title) { 'Help' }
  	let (:heading) { 'Help' }
  end
  describe "About Page" do
    before { visit about_path }
  	let(:page_title) { 'About' }
  	let(:heading) { 'About Us' }
  end
  describe "Contact Page" do
    before { visit contact_path }
  	let(:page_title) { 'Contact' }
  	let(:heading) { 'Contact Us' }
  end

  it "should have the right links on the layout" do
    visit root_path
    click_link "About"
    page.should have_selector 'title', text: full_title('About Us')
    click_link "Help"
    page.should have_selector 'title', text: full_title('Help')
    click_link "Contact"
    page.should have_selector 'title', text: full_title('Contact')
    click_link "Home"
    click_link "Sign Up Now!"
    page.should have_selector 'title', text: full_title('Sign Up')
    click_link "Gabe\'s App"
    page.should have_selector 'h1', text: "Welcome to Gabe\'s App"
  end

end
